{
"name":"Affogato",
"price":"1.2",
"size": "-1"
}

{
"name":"Americano",
"alt_name": "Espresso Americano",
"price":"1.2",
"size": "150"
}

{
"name":"Caffè latte",
"price":"1.2",
"size": "300"
}

{
"name":"Caffè mocha",
"price":"1.2",
"size": "-1"
}

{
"name":"Cafè au lait",
"price":"1.2",
"size": "-1"
}

{
"name":"Cappuccino",
"price":"1.2",
"size": "240"
}

{
"name":"Cold brew coffee",
"price":"1.2",
"size": "-1"
}

{
"name":"Double espresso",
"alt_name": "Doppio",
"price":"1.2",
"size": "60"
}

{
"name":"Espresso",
"price":"1.2",
"size": "30"
}

{
"name":"Espresso con panna",
"price":"1.2",
"size": "30"
}

{
"name":"Espresso macchiato",
"price":"1.2",
"size": "50"
}

{
"name":"Flat white",
"price":"1.2",
"size": "240"
}

{
"name":"Frappé",
"price":"1.2",
"size": "300"
}

{
"name":"Freakshake",
"price":"1.2",
"size": "-1"
}

{
"name":"Iced latte",
"price":"1.2",
"size": "300"
}

{
"name":"Iced mocha",
"price":"1.2",
"size": "300"
}

{
"name":"Irish coffee",
"price":"1.2",
"size": "300"
}

{
"name":"Latte macchiato",
"price":"1.2",
"size": "300"
}

{
"name":"Lungo",
"alt_name": "Espresso lungo",
"price":"1.2",
"size": "60"
}

{
"name":"Ristretto",
"alt_name": "Espresso ristretto",
"price":"1.2",
"size": "20"
}
