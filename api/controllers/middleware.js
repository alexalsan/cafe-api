const jwt = require("jsonwebtoken");

exports.checkToken = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        var decoded = jwt.verify(token, process.env.JWT_KEY);
        req.params.userId = decoded.userId;
        next();
    } catch (error) {
        res.status(500).json({
            message: "Invalid token",
        });
    }
};
