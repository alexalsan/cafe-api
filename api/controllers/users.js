const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const User = require("../models/users");

exports.signup = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then((result) => {
            //We check if the email has already been registered
            if (result.length > 0) {
                return res.status(401).json({
                    message: "Unauthorized operation",
                });
            }

            //We check if it's a valid email
            const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            if (!emailRegex.test(req.body.email)) {
                return res.status(500).json({
                    message: "Invalid email",
                });
            }

            //If everything is correct, we encrypt the password and save the new user
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json(err);
                }

                const user = new User({
                    _id: mongoose.Types.ObjectId(),
                    email: req.body.email,
                    password: hash,
                });

                user.save()
                    .then((result) => {
                        res.status(201).json({
                            message: "Sign up finished correctly",
                            user: {
                                _id: user._id,
                                email: user.email,
                            },
                        });
                    })
                    .catch((error) => {
                        res.status(500).json(error);
                    });
            });
        });
};

exports.login = (req, res, next) => {
    //We find for the email in the DB
    User.find({ email: req.body.email })
        .exec()
        .then((users) => {
            //If the email is not there
            if (users.length < 1) {
                return res.status(401).json({
                    message: "Unauthorized operation",
                });
            }

            //If the email exists, we check if the password is correct
            bcrypt.compare(req.body.password, users[0].password, (err, result) => {
                if (!err && result) {
                    const token = jwt.sign(
                        {
                            email: users[0].email,
                            userId: users[0]._id,
                        },
                        process.env.JWT_KEY,
                        {
                            expiresIn: "1h",
                        }
                    );

                    return res.status(200).json({
                        message: "Auth successful",
                        token: token,
                    });
                }

                return res.status(401).json({
                    message: "Unauthorized operation",
                }); 
            })

            //If the user is valid, we create the JWT and send it
        })
        .catch((error) => {
            res.status(500).json(error);
        });
};
