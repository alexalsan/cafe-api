const mongoose = require("mongoose");
const Product = require("../models/products");

const selectQuery =
    "name alt_name price size intensity image ingredients allergens";

exports.getAllCoffees = (req, res, next) => {
    Product.find({ type: "coffee" })
        .select(selectQuery)
        .exec()
        .then((results) => {
            console.log(results);
            if (results) {
                const response = {
                    count: results.length,
                    products: results.map((result) => {
                        return {
                            product: result,
                            request: {
                                method: "GET",
                                url: `http://${process.env.HOST}:${process.env.PORT}/cafe/${result._id}`,
                            },
                        };
                    }),
                };
                res.status(200).json(response);
            } else {
                res.status(404).json({
                    message: "No products found",
                });
            }
        })
        .catch((err) => {
            res.status(500).json(err);
        });
};

exports.getCoffee = (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id)
        .select(selectQuery)
        .exec()
        .then((result) => {
            if (result) {
                res.status(200).json(result);
            } else {
                res.status(404).json({
                    message: "Product not found",
                });
            }
        })
        .catch((err) => {
            res.status(500).json(err);
        });
};

exports.postCoffee = (req, res, next) => {
    if (!req.body.type || req.body.type === "coffee") {
        const product = new Product({
            _id: mongoose.Types.ObjectId(),
            name: req.body.name,
            alt_name: req.body.alt_name,
            price: req.body.price,
            size: req.body.size, //ml
            image: req.file.path.replace(/\\/gi, "/"),
        });

        product
            .save()
            .then((result) => {
                res.status(201).json({
                    message: "Product created",
                    product: {
                        _id: product._id,
                        name: product.name,
                        alt_name: product.alt_name,
                        price: product.price,
                        size: product.size,
                        image: product.image,
                    },
                });
            })
            .catch((err) => {
                res.status(500).json({
                    error: err,
                });
            });
    } else {
        res.status(500).json({
            message: "Product is not of default type 'coffee'",
            // request: {
            //     method: "POST",
            //     url: `http://${process.env.HOST}:${process.env.PORT}/products/`,
            // },
        });
    }
};

exports.patchCoffee = (req, res, next) => {
    const id = req.params.productId;
    const propertiesToUpdate = {};
    Object.entries(req.body).forEach(([key, value]) => {
        propertiesToUpdate[key] = value;
    });

    Product.updateOne({ _id: id }, { $set: propertiesToUpdate })
        .exec()
        .then((result) => {
            console.log(result);
            res.status(200).json({
                message: "Product updated",
                updated: propertiesToUpdate,
            });
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
};

exports.deleteCoffee = (req, res, next) => {
    const id = req.params.productId;

    Product.deleteOne({ _id: id })
        .exec()
        .then((result) => {
            res.status(200).json({
                message: "Product deleted",
            });
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
};
