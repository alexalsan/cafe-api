const mongoose = require("mongoose");
const Order = require("../models/orders");
const Product = require("../models/products");
const User = require("../models/users");

const selectQuery = "user products";

exports.getAllOrdersFromUser = (req, res, next) => {
    Order.find({ user: req.params.userId })
        .select(selectQuery)
        .populate("products", "name price")
        .exec()
        .then((results) => {
            if (results) {
                const response = {
                    count: results.length,
                    user: req.params.userId, //TODO populate user
                    orders: results.map((result) => {
                        return {
                            order: result,
                            request: {
                                method: "GET",
                                url: `http://${process.env.HOST}:${process.env.PORT}/orders/${result._id}`,
                            },
                        };
                    }),
                };
                res.status(200).json(response);
            } else {
                res.status(404).json({
                    message: "No orders found for user" + req.params.userId,
                });
            }
        })
        .catch((err) => {
            res.status(500).json(err);
        });
};

exports.getOrderFromUser = (req, res, next) => {
    const orderId = req.params.orderId;
    const userId = req.params.userId;

    Order.findById(orderId)
        .select(selectQuery)
        .populate("products", "name price")
        .exec()
        .then((result) => {
            if (result) {
                if (result.user.toString() === userId) {
                    return res.status(200).json({ order: result });
                } else {
                    return res.status(401).json({
                        message: "Unauthorized operation", //Can only access their own orders
                    });
                }
            } else {
                return res.status(404).json({
                    message: "Order not found",
                });
            }
        })
        .catch((err) => {
            res.status(500).json(err);
        });
};

exports.postOrder = (req, res, next) => {
    const userId = req.params.userId;
    const productsId = req.body.products;

    const order = new Order({
        _id: mongoose.Types.ObjectId(),
        user: userId,
        products: productsId,
        date: new Date(),
    });

    order
        .save()
        .then((result) => {
            res.status(201).json({
                message: "Order created",
                order: {
                    result: result,
                },
            });
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
};

exports.patchOrder = (req, res, next) => {
    const orderId = req.params.orderId;
    const userId = req.params.userId;

    const propertiesToUpdate = {};
    Object.entries(req.body).forEach(([key, value]) => {
        propertiesToUpdate[key] = value;
    });

    Order.updateOne(
        { _id: orderId, user: userId },
        { $set: propertiesToUpdate }
    )
        .exec()
        .then((result) => {
            if (result.n) {
                console.log(result);
                res.status(200).json({
                    message: "Order updated",
                    updated: propertiesToUpdate,
                });
            } else {
                return res.status(401).json({
                    message: "Unauthorized operation", //Can only access their own orders
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
};

exports.deleteOrder = (req, res, next) => {
    //A user can only delete their own orders, and ONLY when they are still "pending"

    const orderId = req.params.orderId;
    const userId = req.params.userId;

    const propertiesToUpdate = {};
    Object.entries(req.body).forEach(([key, value]) => {
        propertiesToUpdate[key] = value;
    });

    Order.deleteOne({ _id: orderId, user: userId, status: "pending" })
        .exec()
        .then((result) => {
            if (result.n) {
                res.status(200).json({
                    message: "Order deleted",
                });
            } else {
                return res.status(401).json({
                    message: "Unauthorized operation",
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
};
