const express = require("express");
const router = express.Router();
const cafeControllers = require("../controllers/cafe");
const mdwControllers = require("../controllers/middleware");

//Set up 'multer' for storing images
const multer = require("multer");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "./images/");
    },
    filename: function (req, file, cb) {
        // const prefix = new Date().toISOString().replace(/\:/gi, ''); //file names in Windows can't contain ":"
        cb(null, file.originalname);
    },
});
const upload = multer({ storage: storage });

//Coffee
router.get("/", cafeControllers.getAllCoffees);
router.get("/:productId", cafeControllers.getCoffee);
router.post("/", mdwControllers.checkToken, upload.single("image"), cafeControllers.postCoffee);
router.patch("/:productId", mdwControllers.checkToken, cafeControllers.patchCoffee);
router.delete("/:productId", mdwControllers.checkToken, cafeControllers.deleteCoffee);

module.exports = router;
