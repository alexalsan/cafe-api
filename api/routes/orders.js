const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orders");
const mdwControllers = require("../controllers/middleware");

//Orders
router.get("/",mdwControllers.checkToken, orderControllers.getAllOrdersFromUser);
router.get("/:orderId",mdwControllers.checkToken, orderControllers.getOrderFromUser);
router.post("/", mdwControllers.checkToken, orderControllers.postOrder);
router.patch("/:orderId", mdwControllers.checkToken, orderControllers.patchOrder);
router.delete("/:orderId", mdwControllers.checkToken, orderControllers.deleteOrder);

module.exports = router;
