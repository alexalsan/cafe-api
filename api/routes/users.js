const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/users");

//Users
//router.get("/orders", userControllers.getOrdersFromUser);
//router.patch("/:userId", userControllers.patchUser);
//router.delete("/:userId", userControllers.deleteUser);
router.post("/login", userControllers.login);
router.post("/signup", userControllers.signup);

module.exports = router;
