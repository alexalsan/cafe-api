const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    price: { type: Number, required: true },
    image: { type: String, required: true },
    intensity: { type: Number, default: 2 }, //0 to 4
    type: { type: String, default: "coffee" },
    size: { type: Number }, //ml
    alt_name: { type: String },
    ingredients: [{ type: String }],
    allergens: [{ type: String }],
});

module.exports = mongoose.model("Product", productSchema);
