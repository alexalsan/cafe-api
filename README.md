# CAFE API #

This is a REST API created with Node.js. The main libraries used are Express, Jason Web Token and Multer.
With this API, you can manage your coffee place (not really though ;) ). You can log in (or sign up) with just an email and a password. Then, you can retrieve a list of coffee drinks (the products), and create your orders. Your coffee will be ready in no time !

## How can I access the API?
It is currently not hosted anywhere, but it will hopefully be in the incoming weeks.

### How to use it?
You can do GET, POST, PATCH or DELETE request to both "<host>/products" or "<host>/orders" to retrieve the information. For users, you can do either (POST) "<host>/login." or (POST) "<host>/signup". Loging in will return a JWT, which you will need to attach to the "Authentification" header to access the information.

### ! Detailed usage will be posted once the API is hosted and running.