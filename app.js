//We initialize the app
const express = require("express");
const app = express();
app.listen(process.env.PORT || 3000);
app.use("/images", express.static("images"));

//We connect to the DB
const mongoose = require("mongoose");
// const dbLink = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PWD}@${process.env.DB_NAME}.jjwx0.mongodb.net/cluster0?retryWrites=true&w=majority`;
const dbLink = `mongodb://${process.env.DB_USER}:${process.env.DB_PWD}@cluster0-shard-00-00.jjwx0.mongodb.net:27017,cluster0-shard-00-01.jjwx0.mongodb.net:27017,cluster0-shard-00-02.jjwx0.mongodb.net:27017/${process.env.DB_NAME}?ssl=true&replicaSet=atlas-yp9hkb-shard-0&authSource=admin&retryWrites=true&w=majority`;
mongoose
    .connect(dbLink, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log("OK"))
    .catch((err) => console.log(err));

//We add headers to avoid CORS policy
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header(
            "Access-Control-Allow-Methods",
            "PUT, POST, PATCH, DELETE, GET"
        );
        return res.status(200).json({});
    }
    next();
});

//Parse requests
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//We call the routes
const productRoutes = require("./api/routes/products");
const userRoutes = require("./api/routes/users");
const orderRoutes = require("./api/routes/orders");

app.use("/cafe", productRoutes);
app.use("/users", userRoutes);
app.use("/orders", orderRoutes);

//Throwing a custom 404 error when we can't find the route
app.use((req, res, next) => {
    const error = new Error("Route not found");
    error.status = 404;
    next(error); //We forward this variable to the next middleware
});

//We catch in the following mdw all the errors thrown in our app
app.use((error, req, res, next) => {
    res.status = error.status || 500;
    res.json({
        error: {
            message: error.message,
        },
    });
});
